
import java.io.*;

class Foo {

    String name = "";
    public void geek(String name) { this.name = name; }
}

class GFG {
    public static void main(String[] args)
    {
        Foo ob = new Foo();
        ob.geek("hellobello");
        System.out.println(ob.name);
    }
}
