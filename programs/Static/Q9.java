public class StringTrimExample{
public static void main(String args[]){
String s1="  hello   ";
System.out.println(s1+"how are you");        // without trim()
System.out.println(s1.trim()+"how are you"); // with trim()
}}

10. public class ReplaceExample1{
public static void main(String args[]){
String s1="hello how are you";
String replaceString=s1.replace('h','t');
System.out.println(replaceString); }}
