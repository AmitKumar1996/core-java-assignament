


package com.javacodegeeks.snippets.core;
 
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
 
public class ReadFileInStringWithBufferedInputStream {
 
    public static void main(String[] args) {
         
        File file = new File("inputfile.txt");
         
        BufferedInputStream bin = null;
        FileInputStream fin = null;
 
        try {
             
           
            fin = new FileInputStream(file);
 
       
            bin = new BufferedInputStream(fin);
 
         
            byte[] contents = new byte[1024];
              
            int bytesRead=0;
            String s;
              
            while ((bytesRead = bin.read(contents)) != -1) {
                s = new String(contents, 0, bytesRead);
                System.out.print(s);
            }
 
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        }
        catch (IOException ioe) {
            System.out.println("Exception while reading file " + ioe);
        }
        finally {
            
            try {
                if (fin != null) {
                    fin.close();
                }
                if (bin != null) {
                    bin.close();
                }
            }
            catch (IOException ioe) {
                System.out.println("Error while closing stream : " + ioe);
            }
 
        }
         
    }
 
}
